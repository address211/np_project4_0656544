#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <errno.h>
#include <string.h>
#include <sys/types.h>
#include <time.h>
#include <sys/wait.h>
#include <netdb.h>
#include <sys/stat.h>
#include <fcntl.h>
#define SERV_TCP_PORT 10110
#define BUFFSIZE 8192
struct sockaddr_in	cli_addr;

int err_dump(char* p){
	perror(p);
	exit(1);
}

void data_transfer(int browser_fd,int web_fd){
  unsigned char buffer[BUFFSIZE]={0};
  int nfds=((browser_fd<web_fd)?web_fd:browser_fd)+1,len=0;
  fd_set afds,rfds;
  FD_ZERO(&afds);
	FD_ZERO(&rfds);
  FD_SET(web_fd,&afds);
  FD_SET(browser_fd,&afds);
  while(1){
		len=0;
    memcpy(&rfds,&afds,sizeof(rfds));
    if(select(nfds,&rfds,NULL,NULL,NULL)<0) err_dump("Select error");
    if(FD_ISSET(browser_fd,&rfds)){
      memset(buffer,0,BUFFSIZE);
      len=read(browser_fd,buffer,BUFFSIZE);
      if(len<=0){
        close(web_fd);
        close(browser_fd);
        break;
      }
      else{
        write(web_fd,buffer,len);
      }
    }
    else if(FD_ISSET(web_fd,&rfds)){
      memset(buffer,0,BUFFSIZE);
      len=read(web_fd,buffer,BUFFSIZE);
      if(len<=0){
        close(web_fd);
        close(browser_fd);
        break;
      }
      else{
        write(browser_fd,buffer,len);
      }
    }
  }
}

void proxy(int browser_fd){
  unsigned char request[BUFFSIZE]={0},reply[BUFFSIZE]={0};
  read(browser_fd, request, BUFFSIZE);
  unsigned char VN = request[0];
  unsigned char CD = request[1];
  unsigned int DST_PORT = request[2] << 8 | request[3] ;
  unsigned int DST_IP = request[7] << 24 | request[6] << 16 |
                        request[5] << 8 | request[4] ;
  unsigned char *USER_ID = request + 8 ;
  if(VN!=0x04) exit(0);
  FILE *firewall_fd=fopen("socks.conf","r");
  char rule[10];
  char mode[10];
  char address_str[20];
  unsigned char address[4];
  char *pch;
  reply[1]=0x5B;
  while(!feof(firewall_fd)){
    fscanf(firewall_fd,"%s %s %s\n",rule,mode,address_str);
    pch=strtok(address_str,".");
    address[0]=(unsigned char)atoi(pch);
    pch=strtok(NULL,".");
    address[1]=(unsigned char)atoi(pch);
    pch=strtok(NULL,".");
    address[2]=(unsigned char)atoi(pch);
    pch=strtok(NULL,".");
    address[3]=(unsigned char)atoi(pch);
    if((!strcmp(mode,"c")&&CD==0x01)||(!strcmp(mode,"b")&&CD==0x02)){
      if(((address[0]==request[4])||(address[0]==0x00))&&
        ((address[1]==request[5])||(address[1]==0x00))&&
        ((address[2]==request[6])||(address[2]==0x00))&&
        ((address[3]==request[7])||(address[3]==0x00)))
      {
        reply[1]=0x5A;
        break;
      }
    }
  }
	char str[20]={0};
	inet_ntop(AF_INET,&(cli_addr.sin_addr),str,INET_ADDRSTRLEN);
	printf("*************************************\n");
	printf("<S_IP>:\t%s\n<S_PORT>:%d\t\n",str,(int)cli_addr.sin_port);
	printf("<D_IP>:\t%u.%u.%u.%u\n<D_PORT>:%u\n",request[4],request[5],
				request[6],request[7],DST_PORT);
	printf("<COMMAND>:\t%s\n",(CD==0x01)?"CONNECT":"BIND");
	printf("<REPLY>:\t%s\n",(reply[1]==0x5A)?"Accept":"Reject");
  if(CD==0x01){
		reply[0]=0;
		reply[2]=request[2];
		reply[3]=request[3];
		reply[4]=request[4];
		reply[5]=request[5];
		reply[6]=request[6];
		reply[7]=request[7];
		write(browser_fd,reply,8);
    int web_fd;
		struct sockaddr_in web_addr;
    web_fd=socket(AF_INET,SOCK_STREAM,0);
    bzero((char *) &web_addr, sizeof(web_addr));
    web_addr.sin_family = AF_INET;
    web_addr.sin_addr.s_addr = DST_IP;
    web_addr.sin_port = htons(DST_PORT);
    if(connect(web_fd,(struct sockaddr*)&web_addr,sizeof(web_addr))==-1)
      err_dump("Connect web server fail");
		data_transfer(browser_fd,web_fd);
  }
  else if(CD==0x02){
    int bindfd,ftp_fd;
    socklen_t sa_len,ftp_len;
    struct sockaddr_in bind_addr,sa,ftp_addr;
    if((bindfd=socket(AF_INET,SOCK_STREAM,0))<0)
      err_dump("socket error");
		bzero((char *) &bind_addr, sizeof(bind_addr));
    bind_addr.sin_family=AF_INET;
    bind_addr.sin_addr.s_addr=htonl(INADDR_ANY);
    bind_addr.sin_port=htons(INADDR_ANY);
    if(bind(bindfd,(struct sockaddr*)&bind_addr,sizeof(bind_addr))<0)
      err_dump("bind error");
    sa_len=sizeof(sa);
    if((getsockname(bindfd,(struct sockaddr *)&sa,&sa_len))<0)
      err_dump("getsockname error");
    if(listen(bindfd,5)<0)
			err_dump("listen error");
    reply[0]=0;
    reply[2]=(unsigned char)(ntohs(sa.sin_port)/256);
    reply[3]=(unsigned char)(ntohs(sa.sin_port)%256);
    reply[4]=0;
    reply[5]=0;
    reply[6]=0;
    reply[7]=0;
    write(browser_fd,reply,8);
    ftp_len = sizeof(ftp_addr);
    if((ftp_fd=accept(bindfd,(struct sockaddr *)&ftp_addr,&ftp_len))<0)
      err_dump("accept error");
    write(browser_fd,reply,8);
    data_transfer(browser_fd,ftp_fd);
  }
}

int main(int argc,char *argv[]){
	int	sockfd, newsockfd, childpid;
	struct sockaddr_in serv_addr;
	if((sockfd = socket(AF_INET, SOCK_STREAM, 0))<0)
		err_dump("server: can't open stream socket");

	bzero((char *) &serv_addr, sizeof(serv_addr));
	serv_addr.sin_family = AF_INET;
	serv_addr.sin_addr.s_addr = htonl(INADDR_ANY);
	serv_addr.sin_port = htons(SERV_TCP_PORT);

	int opt=1;
	setsockopt(sockfd, SOL_SOCKET, SO_REUSEADDR,&opt, sizeof(opt));
	if(bind(sockfd, (struct sockaddr *) &serv_addr, sizeof(serv_addr))< 0)
		err_dump("server: can't bind local address");
	listen(sockfd, 5);
	for(;;){
		socklen_t clilen = sizeof(cli_addr);
		newsockfd = accept(sockfd, (struct sockaddr *) &cli_addr, &clilen);
		if(newsockfd < 0) err_dump("server: accept error");
		if((childpid = fork())<0) err_dump("server:fork error");
		else if(childpid == 0){
			close(sockfd);
			proxy(newsockfd);
			exit(0);
		}else{
			close(newsockfd);
		}
	}
}
